import 'package:flutter/material.dart';
import 'package:myquiz/app.dart';
import 'package:quiz_repository/quiz_repository.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final QuizRepository quizRepository = QuizRepository();
  runApp(App(
    quizRepository: quizRepository,
  ));
}
