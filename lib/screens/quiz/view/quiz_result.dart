import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:myquiz/screens/quiz/bloc/quiz_bloc.dart';
import 'package:myquiz/screens/quiz/cubit/quiz_cubit.dart';
import 'package:myquiz/screens/screens.barrel.dart';

class QuizResult extends StatefulWidget {
  const QuizResult({Key? key}) : super(key: key);

  @override
  _QuizResultState createState() => _QuizResultState();
}

class _QuizResultState extends State<QuizResult> {
  @override
  Widget build(BuildContext context) {
    int questionsCount =
        BlocProvider.of<QuizBloc>(context).state.questions!.length;
    return BlocBuilder<QuizCubit, QuizCubitState>(
      buildWhen: (previous, current) => previous != current,
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(),
          body: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: [
                Text(
                  "Your score is ${state.score} of $questionsCount",
                ),
                ElevatedButton(
                  child: const Text('Again'),
                  onPressed: () {
                    context.read<QuizCubit>().clearState();
                    Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const QuizRootScreen(),
                      ),
                    );
                  },
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
