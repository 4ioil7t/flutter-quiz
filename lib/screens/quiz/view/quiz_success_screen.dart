import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:myquiz/screens/quiz/cubit/quiz_cubit.dart';
import 'package:myquiz/screens/screens.barrel.dart';
import 'package:quiz_repository/quiz_repository.dart';

class QuizSuccessScreen extends StatefulWidget {
  const QuizSuccessScreen({Key? key, required this.questions})
      : super(key: key);
  final List<Question>? questions;

  @override
  _QuizSuccessScreenState createState() => _QuizSuccessScreenState();
}

class _QuizSuccessScreenState extends State<QuizSuccessScreen> {
  //PageController _pageController = PageController();

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<QuizCubit, QuizCubitState>(
      listener: (context, state) {},
      buildWhen: (previous, current) => previous != current,
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(),
          body: SafeArea(
            minimum: const EdgeInsets.all(20),
            child: Flexible(
              child: PageView.builder(
                physics: const NeverScrollableScrollPhysics(),
                controller: state.pageController,
                itemBuilder: (context, index) {
                  return QuestionCard(
                    question: widget.questions![index],
                  );
                },
                itemCount: widget.questions!.length,
              ),
            ),
          ),
        );
      },
    );
  }

  // void nextPage() {
  //   _pageController.animateToPage(_pageController.page!.toInt() + 1,
  //       duration: Duration(milliseconds: 400), curve: Curves.easeIn);
  // }

  // void previousPage() {
  //   _pageController.animateToPage(_pageController.page!.toInt() - 1,
  //       duration: Duration(milliseconds: 400), curve: Curves.easeIn);
  // }
}
