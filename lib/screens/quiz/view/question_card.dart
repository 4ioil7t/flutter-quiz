import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:myquiz/screens/quiz/bloc/quiz_bloc.dart';
import 'package:myquiz/screens/quiz/cubit/quiz_cubit.dart';
import 'package:myquiz/screens/screens.barrel.dart';
import 'package:quiz_repository/quiz_repository.dart';

class QuestionCard extends StatelessWidget {
  const QuestionCard({Key? key, required this.question}) : super(key: key);
  final Question question;

  @override
  Widget build(BuildContext context) {
    int questionsCount =
        BlocProvider.of<QuizBloc>(context).state.questions!.length;
    return BlocBuilder<QuizCubit, QuizCubitState>(
      buildWhen: (previous, current) => previous != current,
      builder: (context, state) {
        return Card(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "${(question.id + 1)}",
                      style: const TextStyle(fontSize: 32),
                    ),
                    Text(" /  $questionsCount"),
                  ],
                ),
                const Spacer(
                  flex: 2,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 0),
                  child: Text(
                    question.question,
                    style: const TextStyle(
                      fontSize: 28,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                const Spacer(
                  flex: 1,
                ),
                Container(
                  child: Column(children: [
                    SizedBox(
                      width: double.infinity,
                      child: ElevatedButton(
                        onPressed: () {
                          context.read<QuizCubit>().onAnswerSelected(
                              question.variant1, question, questionsCount);
                          if ((question.id + 1) == questionsCount) {
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                builder: (context) => const QuizResult(),
                              ),
                            );
                          }
                        },
                        child: Container(
                          padding: const EdgeInsets.all(15),
                          child: Text(
                            question.variant1,
                            style: const TextStyle(fontSize: 24),
                          ),
                        ),
                      ),
                    ),
                    const Padding(padding: EdgeInsets.only(bottom: 20)),
                    SizedBox(
                      width: double.infinity,
                      child: ElevatedButton(
                        onPressed: () {
                          context.read<QuizCubit>().onAnswerSelected(
                              question.variant2, question, questionsCount);
                          if ((question.id + 1) == questionsCount) {
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                builder: (context) => const QuizResult(),
                              ),
                            );
                          }
                        },
                        child: Container(
                          padding: const EdgeInsets.all(15),
                          child: Text(
                            question.variant2,
                            style: const TextStyle(fontSize: 24),
                          ),
                        ),
                      ),
                    ),
                    const Padding(padding: EdgeInsets.only(bottom: 20)),
                    SizedBox(
                      width: double.infinity,
                      child: ElevatedButton(
                        onPressed: () {
                          context.read<QuizCubit>().onAnswerSelected(
                              question.variant3, question, questionsCount);
                          if ((question.id + 1) == questionsCount) {
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                builder: (context) => const QuizResult(),
                              ),
                            );
                          }
                        },
                        child: Container(
                          padding: const EdgeInsets.all(15),
                          child: Text(
                            question.variant3,
                            style: const TextStyle(fontSize: 24),
                          ),
                        ),
                      ),
                    ),
                  ]),
                ),
                const Spacer(
                  flex: 2,
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
