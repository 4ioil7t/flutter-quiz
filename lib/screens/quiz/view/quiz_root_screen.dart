import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:myquiz/screens/quiz/bloc/quiz_bloc.dart';
import 'package:myquiz/screens/screens.barrel.dart';

class QuizRootScreen extends StatefulWidget {
  const QuizRootScreen({Key? key}) : super(key: key);

  @override
  _QuizRootScreenState createState() => _QuizRootScreenState();
}

class _QuizRootScreenState extends State<QuizRootScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<QuizBloc, QuizState>(
      buildWhen: (previous, current) =>
          current.status == QuizStatus.success ||
          current.status == QuizStatus.error ||
          current.status == QuizStatus.loading,
      builder: (context, state) {
        switch (state.status) {
          case QuizStatus.loading:
            return const LoadingScreen();
          case QuizStatus.success:
            return QuizSuccessScreen(questions: state.questions);
          case QuizStatus.error:
            return const Center(child: Icon(Icons.cancel_outlined));
          default:
            return const SizedBox();
        }
      },
    );
  }
}
