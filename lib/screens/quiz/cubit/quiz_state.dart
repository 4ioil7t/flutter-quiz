part of 'quiz_cubit.dart';

class QuizCubitState extends Equatable {
  const QuizCubitState({
    this.score = 0,
    this.currQuestion,
    this.selectedAnswer,
    this.isAnswered = false,
    required PageController controller,
  }) : pageController = controller;

  final int score;
  final Question? currQuestion;
  final String? selectedAnswer;
  final bool isAnswered;
  final PageController pageController;

  QuizCubitState copyWith({
    int? score,
    Question? currQuestion,
    String? selectedAnswer,
    bool? isAnswered,
    PageController? pageController,
  }) {
    return QuizCubitState(
      score: score ?? this.score,
      currQuestion: currQuestion ?? this.currQuestion,
      selectedAnswer: selectedAnswer ?? this.selectedAnswer,
      isAnswered: isAnswered ?? this.isAnswered,
      controller: pageController ?? this.pageController,
    );
  }

  @override
  List<Object?> get props => [
        score,
        currQuestion,
        selectedAnswer,
        isAnswered,
        pageController,
      ];
}
