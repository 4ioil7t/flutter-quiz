import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:quiz_repository/quiz_repository.dart';

part 'quiz_state.dart';

class QuizCubit extends Cubit<QuizCubitState> {
  QuizCubit() : super(QuizCubitState(controller: PageController()));

  Future<void> onAnswerSelected(
      String selectedAnswer, Question currQuestion, int questionCount) async {
    if (selectedAnswer == currQuestion.answer) {
      emit(
        state.copyWith(
          score: state.score + 1,
          isAnswered: true,
        ),
      );
    }
    if ((currQuestion.id + 1) != questionCount) {
      await Future.delayed(const Duration(seconds: 1), () {
        emit(state.copyWith(isAnswered: false));
        state.pageController.nextPage(
            duration: const Duration(milliseconds: 200), curve: Curves.ease);
      });
    }
  }

  void clearState() {
    emit(
      state.copyWith(
        score: 0,
        currQuestion: null,
        selectedAnswer: null,
        isAnswered: false,
        pageController: PageController(),
      ),
    );
  }
}
