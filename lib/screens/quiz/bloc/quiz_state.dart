part of 'quiz_bloc.dart';

enum QuizStatus { initial, success, error, loading }

extension RequestsStatusGetter on QuizStatus {
  bool get isInitial => this == QuizStatus.initial;
  bool get isSuccess => this == QuizStatus.success;
  bool get isError => this == QuizStatus.error;
  bool get isLoading => this == QuizStatus.loading;
}

class QuizState extends Equatable {
  const QuizState({
    this.questions,
    this.error,
    this.status = QuizStatus.initial,
  });

  final QuizStatus status;
  final List<Question>? questions;
  final String? error;

  QuizState copyWith({
    List<Question>? questions,
    QuizStatus? status,
    String? error,
  }) {
    return QuizState(
      questions: questions ?? this.questions,
      status: status ?? this.status,
      error: error ?? this.error,
    );
  }

  @override
  List<Object?> get props => [
        questions,
        status,
        error,
      ];
}
