import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:quiz_repository/quiz_repository.dart';

part 'quiz_event.dart';
part 'quiz_state.dart';

class QuizBloc extends Bloc<QuizEvent, QuizState> {
  QuizBloc({required QuizRepository quizRepository})
      : _quizRepository = quizRepository,
        super(const QuizState()) {
    on<QuizLoaded>(_onRequestsLoaded);
  }
  final QuizRepository _quizRepository;

  Future<void> _onRequestsLoaded(
      QuizLoaded event, Emitter<QuizState> emit) async {
    emit(state.copyWith(status: QuizStatus.loading));
    try {
      final List<Question> questionsList = await _quizRepository.getQuestions();

      debugPrint(questionsList.toString());

      emit(
        state.copyWith(
          status: QuizStatus.success,
          questions: questionsList,
        ),
      );
    } catch (error) {
      debugPrint(error.toString());
      emit(
        state.copyWith(
          status: QuizStatus.error,
          error: error.toString(),
        ),
      );
    }
  }
}
