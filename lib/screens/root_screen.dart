import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:myquiz/common/blocs/bottom_bar/bottom_bar_cubit.dart';
import 'package:myquiz/common/widgets/scaffold.dart';
import 'package:myquiz/screens/screens.barrel.dart';

class RootScreen extends StatefulWidget {
  const RootScreen({Key? key}) : super(key: key);
  static Route<void> route() {
    return MaterialPageRoute<void>(builder: (context) => const RootScreen());
  }

  @override
  State<StatefulWidget> createState() => RootScreenState();
}

class RootScreenState extends State<RootScreen> {
  @override
  Widget build(BuildContext context) {
    return MainScaffold(
      body: SafeArea(
        child: BlocBuilder<BottomNavigationCubit, BottomNavigationState>(
          builder: (context, state) {
            if (state.navbarItem == BottomNavigationItem.search) {
              return const Center(
                child: Text('Search Screen'),
              );
            } else if (state.navbarItem == BottomNavigationItem.home) {
              return const Center(
                child: Text('Home Screen'),
              );
            } else if (state.navbarItem == BottomNavigationItem.courses) {
              return const Center(
                child: Text('Courses Screen'),
              );
            }
            return Container();
          },
        ),
      ),
      bottomNavigationBar:
          BlocBuilder<BottomNavigationCubit, BottomNavigationState>(
        builder: (context, state) {
          return SizedBox(
            height: 65,
            child: BottomAppBar(
              shape: const CircularNotchedRectangle(),
              notchMargin: 8.0,
              clipBehavior: Clip.antiAlias,
              child: Row(
                //children inside bottom appbar
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: const EdgeInsets.fromLTRB(0, 10, 5, 5),
                    width: 70,
                    child: InkWell(
                      radius: 10,
                      borderRadius: BorderRadius.circular(10),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(
                            Icons.search,
                            size: 32,
                            color:
                                state.navbarItem == BottomNavigationItem.search
                                    ? Theme.of(context)
                                        .bottomNavigationBarTheme
                                        .selectedItemColor
                                    : Theme.of(context)
                                        .bottomNavigationBarTheme
                                        .unselectedItemColor,
                          ),
                          AnimatedCrossFade(
                            firstCurve: Curves.bounceInOut,
                            secondCurve: Curves.easeInBack,
                            firstChild: Text(
                              'Поиск',
                              style: Theme.of(context)
                                  .bottomNavigationBarTheme
                                  .selectedLabelStyle,
                            ),
                            secondChild: const SizedBox(),
                            crossFadeState:
                                state.navbarItem == BottomNavigationItem.search
                                    ? CrossFadeState.showFirst
                                    : CrossFadeState.showSecond,
                            duration: const Duration(milliseconds: 80),
                          ),
                        ],
                      ),
                      onTap: () {
                        context
                            .read<BottomNavigationCubit>()
                            .changeBottomNavigationItem(
                                BottomNavigationItem.search);
                      },
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.fromLTRB(5, 10, 0, 5),
                    width: 70,
                    child: InkWell(
                      radius: 10,
                      borderRadius: BorderRadius.circular(10),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(
                            Icons.list_rounded,
                            size: 32,
                            color:
                                state.navbarItem == BottomNavigationItem.courses
                                    ? Theme.of(context)
                                        .bottomNavigationBarTheme
                                        .selectedItemColor
                                    : Theme.of(context)
                                        .bottomNavigationBarTheme
                                        .unselectedItemColor,
                          ),
                          AnimatedCrossFade(
                            firstCurve: Curves.bounceInOut,
                            secondCurve: Curves.easeInBack,
                            firstChild: Text(
                              'Курсы',
                              style: Theme.of(context)
                                  .bottomNavigationBarTheme
                                  .selectedLabelStyle,
                            ),
                            secondChild: const SizedBox(),
                            crossFadeState:
                                state.navbarItem == BottomNavigationItem.courses
                                    ? CrossFadeState.showFirst
                                    : CrossFadeState.showSecond,
                            duration: const Duration(milliseconds: 80),
                          ),
                        ],
                      ),
                      onTap: () {
                        context
                            .read<BottomNavigationCubit>()
                            .changeBottomNavigationItem(
                                BottomNavigationItem.courses);
                      },
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
      floatingActionButtonLocation:
          FloatingActionButtonLocation.miniCenterDocked,
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(6.0),
        child: BlocBuilder<BottomNavigationCubit, BottomNavigationState>(
          builder: (context, state) {
            return FloatingActionButton(
              backgroundColor: state.navbarItem == BottomNavigationItem.home
                  ? Theme.of(context).bottomNavigationBarTheme.selectedItemColor
                  : Colors.white,
              child: Icon(
                Icons.home,
                color: state.navbarItem == BottomNavigationItem.home
                    ? Colors.white
                    : Theme.of(context)
                        .bottomNavigationBarTheme
                        .selectedItemColor,
              ),
              onPressed: () => setState(
                () {
                  context
                      .read<BottomNavigationCubit>()
                      .changeBottomNavigationItem(BottomNavigationItem.home);
                },
              ),
            );
          },
        ),
      ),
    );
  }
}
