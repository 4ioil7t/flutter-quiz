import 'dart:async';
import 'package:flutter/material.dart';
import 'package:myquiz/screens/screens.barrel.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return SplashState();
  }
}

class SplashState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    startTime();
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      resizeToAvoidBottomInset: true,
      body: Logo(),
    );
  }

  route() {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => const RootScreen()));
  }

  startTime() async {
    var duration = const Duration(seconds: 3);
    return Timer(duration, route);
  }
}
