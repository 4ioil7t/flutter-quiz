import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:user_repository/user_repository.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  static Route<void> route() {
    return MaterialPageRoute<void>(builder: (context) => const ProfileScreen());
  }

  @override
  Widget build(BuildContext context) {
    User user = UserMock().user;
    return Scaffold(
      appBar: AppBar(),
      body: SafeArea(
        minimum: const EdgeInsets.fromLTRB(15, 15, 15, 0),
        child: SizedBox(
          width: double.infinity,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              AccountInfoHeader(user: user),
            ],
          ),
        ),
      ),
    );
  }
}

class AccountInfoHeader extends StatelessWidget {
  const AccountInfoHeader({Key? key, required this.user}) : super(key: key);

  final User user;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: [
        Container(
          constraints: const BoxConstraints(maxHeight: 81.0, maxWidth: 95.0),
          margin: const EdgeInsets.only(bottom: 15),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
              fit: BoxFit.fill,
              image: AssetImage("assets/${user.photo}"),
            ),
          ),
        ),
        Text(
          (user.name + ' ' + user.surname),
          style: Theme.of(context).textTheme.headline5,
        ),
      ],
    );
  }
}
