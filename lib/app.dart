import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:myquiz/common/app_theme.dart';
import 'package:myquiz/common/blocs/bottom_bar/bottom_bar_cubit.dart';
import 'package:myquiz/screens/quiz/bloc/quiz_bloc.dart';
import 'package:myquiz/screens/quiz/cubit/quiz_cubit.dart';
import 'package:myquiz/screens/screens.barrel.dart';
import 'package:quiz_repository/quiz_repository.dart';

class App extends StatelessWidget {
  const App({required QuizRepository quizRepository})
      : _quizRepository = quizRepository,
        super(key: const Key('app'));
  final QuizRepository _quizRepository;

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider.value(value: _quizRepository),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider<QuizBloc>(
            create: (context) => QuizBloc(
              quizRepository: _quizRepository,
            )..add(QuizLoaded()),
          ),
          BlocProvider(
            create: (context) => QuizCubit(),
          ),
          BlocProvider<BottomNavigationCubit>(
            create: (context) => BottomNavigationCubit(),
          ),
        ],
        child: const MyApp(),
      ),
    );
  }
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: AppTheme().theme,
      home: const SplashScreen(),
    );
  }
}
