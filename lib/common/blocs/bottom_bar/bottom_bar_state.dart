part of 'bottom_bar_cubit.dart';

enum BottomNavigationItem { search, home, courses }

@immutable
class BottomNavigationState extends Equatable {
  final BottomNavigationItem navbarItem;
  final int index;

  const BottomNavigationState(this.navbarItem, this.index);

  @override
  List<Object> get props => [navbarItem, index];
}
