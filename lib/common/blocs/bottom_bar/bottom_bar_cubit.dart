import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

part 'bottom_bar_state.dart';

class BottomNavigationCubit extends Cubit<BottomNavigationState> {
  BottomNavigationCubit()
      : super(const BottomNavigationState(BottomNavigationItem.home, 2));

  void changeBottomNavigationItem(BottomNavigationItem navbarItem) {
    switch (navbarItem) {
      case BottomNavigationItem.search:
        emit(const BottomNavigationState(BottomNavigationItem.search, 0));
        break;
      case BottomNavigationItem.courses:
        emit(const BottomNavigationState(BottomNavigationItem.courses, 1));
        break;
      case BottomNavigationItem.home:
        emit(const BottomNavigationState(BottomNavigationItem.home, 2));
        break;
    }
  }
}
