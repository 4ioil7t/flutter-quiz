import 'package:flutter/material.dart';

class AppTheme {
  ThemeData theme = ThemeData(
    colorScheme: const ColorScheme(
      brightness: Brightness.light,
      primary: Colors.white,
      onPrimary: Colors.white,
      secondary: Color(0xFF764AF1),
      onSecondary: Color(0xFF764AF1),
      surface: Colors.white,
      onSurface: Colors.white,
      background: Colors.white,
      onBackground: Colors.white,
      error: Colors.redAccent,
      onError: Colors.redAccent,
    ),
    textTheme: const TextTheme(
      headline1: TextStyle(
          fontSize: 28, fontWeight: FontWeight.bold, color: Colors.blueGrey),
      headline2: TextStyle(
          fontSize: 21, fontWeight: FontWeight.normal, color: Colors.black),
      headline3: TextStyle(
          fontSize: 16, fontWeight: FontWeight.w500, color: Colors.black),
      headline4: TextStyle(
          fontSize: 16, fontWeight: FontWeight.bold, color: Colors.black),
      subtitle2: TextStyle(color: Colors.grey, fontSize: 14),
    ),
    iconTheme: const IconThemeData(color: Colors.black, size: 35),
    //scaffoldBackgroundColor: const Color(0xFF3C415C),
    dividerTheme: const DividerThemeData(
      color: Colors.grey,
      thickness: 0.6,
    ),
    inputDecorationTheme: InputDecorationTheme(
      iconColor: MaterialStateColor.resolveWith(
        (Set<MaterialState> states) {
          if (states.contains(MaterialState.focused)) {
            return const Color(0xFF875F24);
          }
          if (states.contains(MaterialState.error)) {
            return Colors.redAccent;
          }
          return Colors.grey;
        },
      ),
      enabledBorder: const UnderlineInputBorder(
          borderSide: BorderSide(
              color: Colors.grey, width: 0.6, style: BorderStyle.solid)),
      focusedBorder: const UnderlineInputBorder(
          borderSide: BorderSide(
              color: Color(0xFFE9B362), width: 0.6, style: BorderStyle.solid)),
    ),
    cardTheme: const CardTheme(
      color: Color(0xFFD8D2CB),
      clipBehavior: Clip.antiAliasWithSaveLayer,
      shadowColor: Color.fromARGB(212, 212, 212, 212),
      elevation: 8,
    ),
    appBarTheme: const AppBarTheme(
      backgroundColor: Color(0xFF764AF1),
      iconTheme: IconThemeData(color: Color(0xFFF2F2F2), size: 32),
      //titleTextStyle: TextStyle(color: Colors.black),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
        textStyle: const TextStyle(fontSize: 16),
        padding: const EdgeInsets.all(20),
        primary: const Color(0xFFE9B362),
        minimumSize: const Size(double.maxFinite, double.minPositive),
      ),
    ),
    bottomNavigationBarTheme: const BottomNavigationBarThemeData(
      //backgroundColor: Color(0xFFB4A5A5),
      type: BottomNavigationBarType.fixed,
      showSelectedLabels: true,
      showUnselectedLabels: false,
      unselectedItemColor: Color.fromARGB(255, 200, 200, 200),
      selectedItemColor: Color(0xFF764AF1),
      selectedLabelStyle:
          TextStyle(color: Color(0xFF764AF1), fontWeight: FontWeight.bold),
      //unselectedLabelStyle: TextStyle(color: Colors.black),
      unselectedIconTheme:
          IconThemeData(color: Color.fromARGB(255, 200, 200, 200), size: 32),
      //selectedLabelStyle: TextStyle(color: Colors.black),
      selectedIconTheme: IconThemeData(color: Color(0xFF764AF1), size: 32),
    ),
  );
}
