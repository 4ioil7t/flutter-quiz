import 'package:flutter/material.dart';
import 'package:myquiz/screens/profile/view/view.dart';
import 'package:user_repository/user_repository.dart';

class MainScaffold extends StatelessWidget {
  const MainScaffold({
    Key? key,
    this.title,
    this.body,
    this.appBarBottom,
    this.additionalTopActions = const [],
    this.bottomNavigationBar,
    this.floatingActionButtonLocation,
    this.floatingActionButton,
  }) : super(key: key);

  final String? title;
  final Widget? body;
  final PreferredSizeWidget? appBarBottom;
  final List<Widget> additionalTopActions;
  final Widget? bottomNavigationBar;
  final Widget? floatingActionButton;
  final FloatingActionButtonLocation? floatingActionButtonLocation;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        actions: [
          IconButton(
            icon: const Icon(Icons.exit_to_app),
            onPressed: () {},
          ),
        ],
        title: Text(title ?? ''),
      ),
      drawer: MainDrawer(),
      body: body,
      bottomNavigationBar: bottomNavigationBar,
      floatingActionButtonLocation: floatingActionButtonLocation,
      floatingActionButton: floatingActionButton,
    );
  }
}

class MainDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          MainDrawerHeader(),
          Expanded(
            child: Container(
              //color: Color(0xFF04293A),
              child: ListView(
                padding: const EdgeInsets.all(0),
                children: [
                  ListTile(
                    leading: const Icon(
                      Icons.person,
                      size: 30,
                    ),
                    title: const Text('Профиль'),
                    onTap: () {
                      Navigator.of(context).push(ProfileScreen.route());
                    },
                  ),
                  ListTile(
                    leading: const Icon(
                      Icons.payment_rounded,
                      size: 30,
                    ),
                    title: const Text('Платежная информация'),
                    onTap: () {
                      //Navigator.of(context).push(CatalogScreen.route());
                    },
                  ),
                  ListTile(
                    leading: const Icon(
                      Icons.align_vertical_bottom,
                      size: 30,
                    ),
                    title: const Text('Статистика'),
                    onTap: () {},
                  ),
                  ListTile(
                    leading: const Icon(
                      Icons.schedule_rounded,
                      size: 30,
                    ),
                    title: const Text('График'),
                    onTap: () {
                      //Navigator.of(context).push(NewsScreen.route());
                    },
                  ),
                  ListTile(
                    leading: const Icon(
                      Icons.help,
                      size: 30,
                    ),
                    title: const Text('Связь с поддержкой'),
                    onTap: () {
                      //Navigator.of(context).push(ConsultingScreen.route());
                    },
                  ),
                ],
              ),
            ),
          ),
          Divider(height: 1.0),
          Container(
            //color: Color(0xFF04293A),
            child: ListTile(
              leading: const Icon(
                Icons.exit_to_app,
                size: 30,
              ),
              title: const Text('Выйти'),
              onTap: () {},
            ),
          ),
        ],
      ),
    );
  }
}

class MainDrawerHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (context) {
        final User user = UserMock().user;
        return UserAccountsDrawerHeader(
          decoration: const BoxDecoration(
            color: Color(0xFF764AF1),
          ),
          margin: const EdgeInsets.all(0),
          accountName: Text(
            user.name + ' ' + user.surname,
            style: const TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 18.0,
            ),
          ),
          accountEmail: Text(user.email),
          currentAccountPicture: Container(
            constraints: const BoxConstraints(maxHeight: 81.0, maxWidth: 95.0),
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                fit: BoxFit.fill,
                image: AssetImage("assets/${user.photo}"),
              ),
            ),
          ),
        );
      },
    );
  }
}
