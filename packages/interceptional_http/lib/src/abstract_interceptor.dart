/**
 * @desc абстрактный класс для создания разных типов интерсепторов с помощью дженериков
 */
abstract class AbstractInterceptor<T> {
  Future<T> intercept(T input);
}
