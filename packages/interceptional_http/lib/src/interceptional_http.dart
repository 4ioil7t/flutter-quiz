import 'package:http/http.dart' as http;
import 'package:interceptional_http/interceptional_http.dart';

class InterceptionalHttp {
  InterceptionalHttp(http.Client? httpClient)
      : _httpClient = httpClient ?? http.Client(),
        preInterceptors = [],
        postInterceptors = [];

  final http.Client _httpClient;
  /**
   * @prop pre_interceptors
   * @desc для хранения интерсепторов для обработки запроса до его отправления 
   */
  final List<AbstractInterceptor<Request>> preInterceptors;
  /**
   * @prop post_interceptors
   * @desc для хранения интерсепторов для обработки ответа после его получения 
   */
  final List<AbstractInterceptor<http.Response>> postInterceptors;

  /**
   * @desc функция обработки запроса до отправки
   * после получения ответа вызывает функцию его обработки
   */
  Future<http.Response> get(Request request) async {
    http.Response result;
    try {
      for (var interceptor in preInterceptors) {
        request = await interceptor.intercept(request);
      }
      result =
          await this._httpClient.get(request.uri, headers: request.headers);
    } catch (e) {
      // обработка всех ошибок
      // выкидываем ошибку еще выше
      throw e;
    }

    return this._tap(result);
  }

  /**
   * функция обработки ответа через пост-интерсепторы
   */
  Future<http.Response> _tap(http.Response res) async {
    for (var interceptor in postInterceptors) {
      res = await interceptor.intercept(res);
    }
    return res;
  }

  /** @todo дописать методы класса http */
}
