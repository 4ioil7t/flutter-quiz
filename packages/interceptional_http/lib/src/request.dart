class Request {
  Request({required this.uri, Object? body, Map<String, String>? headers});

  final Uri uri;
  Object? body;
  Map<String, String>? headers;
}
