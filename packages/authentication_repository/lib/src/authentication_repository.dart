import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'package:http/http.dart' as http;
import 'package:interceptional_http/interceptional_http.dart';

enum AuthenticationStatus { unknown, authenticated, unauthenticated }

class AuthenticationRequestFailure implements Exception {
  AuthenticationRequestFailure(
      {String message = 'Authentication repository error'})
      : this._message = message;

  String _message;

  @override
  String toString() {
    return _message;
  }
}

class AuthenticationRepository {
  AuthenticationRepository(
      {required this.baseUrl,
      required List<AbstractInterceptor<Request>> preInterceptors,
      required List<AbstractInterceptor<http.Response>> postInterceptors,
      required this.initialStatusGetter,
      required this.logoutStatusGetter}) {
    httpClient.preInterceptors.addAll(preInterceptors);
    httpClient.postInterceptors.addAll(postInterceptors);
  }

  final String baseUrl;
  final Future<AuthenticationStatus> Function() initialStatusGetter;
  final Future<AuthenticationStatus> Function() logoutStatusGetter;
  final InterceptionalHttp httpClient = InterceptionalHttp(http.Client());
  final StreamController<AuthenticationStatus> _controller =
      StreamController<AuthenticationStatus>();

  Stream<AuthenticationStatus> get status async* {
    //await Future<void>.delayed(const Duration(seconds: 1));
    //yield AuthenticationStatus.unauthenticated;
    //yield* _controller.stream;
    yield await initialStatusGetter();
    yield* _controller.stream;
  }

  Future<void> logIn({
    required String email,
    required String password,
  }) async {
    try {
      Map<String, String> data = {'email': email, 'password': password};
      String content = jsonEncode(data);

      final Request request = Request(
        uri: Uri.https(
          baseUrl,
          '/api/user/signin',
          <String, dynamic>{
            'login': email,
            'password': password,
            'type': 'classic',
          },
        ),
        body: content,
      );

      final response = await this.httpClient.get(request);

      if (response.statusCode != 200) {
        throw AuthenticationRequestFailure();
      }

      _controller.add(AuthenticationStatus.authenticated);
    } catch (e) {
      log(AuthenticationRequestFailure().toString());
      throw new AuthenticationRequestFailure(message: e.toString());
    }
  }

  void logOut() async {
    AuthenticationStatus logoutStatus = await logoutStatusGetter();
    _controller.add(logoutStatus);
  }

  void dispose() => _controller.close();
}
