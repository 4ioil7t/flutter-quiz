class LoginDTO {
  /** @todo: сделать декоратор */
  LoginDTO(this.login, this.jwt, this.access_token, this.refresh_token);
  factory LoginDTO.fromJson(dynamic json) {
    return LoginDTO(json['login'] as String, json['jwt'] as String,
        json['access_token'] as String, json['refresh_token'] as String);
  }

  String login;
  String jwt;
  String access_token;
  String refresh_token;

  @override
  String toString() {
    return '{ ${this.login}, ${this.jwt}, ${this.access_token}, ${this.refresh_token} }';
  }

  bool validate() {
    return true;
  }
}
