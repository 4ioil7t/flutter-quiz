import 'package:equatable/equatable.dart';

class User extends Equatable {
  const User({
    required this.name,
    required this.surname,
    required this.email,
    required this.phone,
    this.id,
    this.photo,
  });

  final String name;
  final String surname;
  final String email;
  final String phone;

  final String? id;
  final String? photo;

  @override
  List<Object?> get props => [
        name,
        surname,
        email,
        phone,
        id,
        photo,
      ];

  static const empty = User(
    name: '',
    surname: '',
    email: '',
    phone: '',
  );
}

class UserMock {
  User user = User(
    name: 'Olga',
    surname: 'Kazantseva',
    email: 'example@gmail.com',
    phone: '+7 999 123 45 67',
    photo: 'pepeFrog.jpg',
  );
}
