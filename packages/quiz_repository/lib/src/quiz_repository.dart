import 'dart:async';
import 'dart:developer';

import 'package:quiz_repository/quiz_repository.dart';

class QuizRequestFailure implements Exception {
  QuizRequestFailure({String message = 'Request repository error'})
      : this._message = message;

  String _message;

  @override
  String toString() {
    return _message;
  }
}

class QuizRepository {
  Future<List<Question>> getQuestions() async {
    List<Question> questions = <Question>[];
    try {
      await QuizDatabase.instance.create(
        const Question(
          id: 0,
          question: 'Сколько спутников у Марса?',
          answer: '2',
          variant1: '13',
          variant2: '2',
          variant3: '1',
        ),
      );
      await QuizDatabase.instance.create(
        const Question(
          id: 1,
          question:
              'Самый большой вулкан Солнечной системы называется «Гора Олимп». Где он находится?',
          answer: 'Марс',
          variant1: 'Марс',
          variant2: 'Юпитер',
          variant3: 'Меркурий',
        ),
      );
      await QuizDatabase.instance.create(
        const Question(
          id: 2,
          question: 'Какая планета ближе всех расположена к Солнцу?',
          answer: 'Венера',
          variant1: 'Венера',
          variant2: 'Плутон',
          variant3: 'Нептун',
        ),
      );
      questions = await QuizDatabase.instance.readAllQuestions();
    } catch (e) {
      log(new QuizRequestFailure().toString());
      throw new QuizRequestFailure(message: e.toString());
    }
    return questions;
  }
}
