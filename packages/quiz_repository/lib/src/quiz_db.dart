import 'package:path/path.dart';
import 'package:quiz_repository/src/models/models.dart';
import 'package:sqflite/sqflite.dart';

class QuizDatabase {
  static final QuizDatabase instance = QuizDatabase._init();

  static Database? _database;

  QuizDatabase._init();

  Future<Database> get database async {
    if (_database != null) return _database!;

    _database = await _initDB('quiz_db.db');
    return _database!;
  }

  Future<Database> _initDB(String filePath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filePath);

    return await openDatabase(path, version: 1, onCreate: _createDB);
  }

  Future _createDB(Database db, int version) async {
    final idType = 'INTEGER PRIMARY KEY';
    final textType = 'TEXT NOT NULL';
    final boolType = 'BOOLEAN NOT NULL';
    final integerType = 'INTEGER NOT NULL';

    await db.execute('''
CREATE TABLE $tableName ( 
  ${QuestionFields.id} $idType, 
  ${QuestionFields.question} $textType,
  ${QuestionFields.answer} $textType,
  ${QuestionFields.variant1} $textType,
  ${QuestionFields.variant2} $textType,
  ${QuestionFields.variant3} $textType
  )
''');
  }

  Future<Question> create(Question question) async {
    final db = await instance.database;

    final id = await db.insert(
      tableName,
      question.toJson(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
    return question.copy(id: id);
  }

  Future<Question> readQuestion(int id) async {
    final db = await instance.database;

    final maps = await db.query(
      tableName,
      columns: QuestionFields.values,
      where: '${QuestionFields.id} = ?',
      whereArgs: [id],
    );

    print(maps);

    if (maps.isNotEmpty) {
      print(Question.fromJson(maps.first));
      return Question.fromJson(maps.first);
    } else {
      throw Exception('ID $id not found');
    }
  }

  Future<List<Question>> readAllQuestions() async {
    final db = await instance.database;
    final result = await db.query(tableName);

    return result.map((json) => Question.fromJson(json)).toList();
  }

  Future<int> update(Question question) async {
    final db = await instance.database;

    return db.update(
      tableName,
      question.toJson(),
      where: '${QuestionFields.id} = ?',
      whereArgs: [question.id],
    );
  }

  Future<int> delete(int id) async {
    final db = await instance.database;

    return await db.delete(
      tableName,
      where: '${QuestionFields.id} = ?',
      whereArgs: [id],
    );
  }

  Future close() async {
    final db = await instance.database;

    db.close();
  }
}
