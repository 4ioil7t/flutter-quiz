final String tableName = 'questions';

class QuestionFields {
  static final List<String> values = [
    id,
    question,
    answer,
    variant1,
    variant2,
    variant3,
  ];

  static final String id = '_id';
  static final String question = 'question';
  static final String answer = 'answer';
  static final String variant1 = 'variant1';
  static final String variant2 = 'variant2';
  static final String variant3 = 'variant3';
}

class Question {
  const Question({
    required this.id,
    required this.question,
    required this.answer,
    required this.variant1,
    required this.variant2,
    required this.variant3,
  });

  final int id;
  final String question;
  final String answer;
  final String variant1;
  final String variant2;
  final String variant3;

  Question copy({
    int? id,
    String? question,
    String? answer,
    String? variant1,
    String? variant2,
    String? variant3,
  }) =>
      Question(
        id: id ?? this.id,
        question: question ?? this.question,
        answer: answer ?? this.answer,
        variant1: variant1 ?? this.variant1,
        variant2: variant2 ?? this.variant2,
        variant3: variant3 ?? this.variant3,
      );

  static Question fromJson(Map<String, Object?> json) => Question(
        id: json[QuestionFields.id] as int,
        question: json[QuestionFields.question] as String,
        answer: json[QuestionFields.answer] as String,
        variant1: json[QuestionFields.variant1] as String,
        variant2: json[QuestionFields.variant2] as String,
        variant3: json[QuestionFields.variant3] as String,
      );

  Map<String, Object?> toJson() => {
        QuestionFields.id: id,
        QuestionFields.question: question,
        QuestionFields.answer: answer,
        QuestionFields.variant1: variant1,
        QuestionFields.variant2: variant2,
        QuestionFields.variant3: variant3,
      };

  @override
  String toString() {
    return 'Dog{id: $id, question: $question, answer: $answer, variant1: $variant1, variant2: $variant2, variant3: $variant3,}';
  }
}
